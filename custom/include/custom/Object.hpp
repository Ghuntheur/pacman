#pragma once

#include "VAO.hpp"
#include "VBO.hpp"
/*
 * Object is a super class with buffers
 */
class Object {
protected:
    VBO m_vbo;
    VAO m_vao;

public:
    Object() {}
    virtual ~Object() {}
    void init();
    void removeBuffers();
    void draw();
    virtual GLsizei getVertexCount() const = 0;
    virtual const void * getVertex() const = 0;
};