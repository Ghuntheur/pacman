#pragma once

#include <vector>

#include <glimac/common.hpp>
#include <custom/VAO.hpp>
#include <custom/VBO.hpp>
#include "Object.hpp"

class Cube : public Object {

    void build();

public:

    explicit Cube(float length = 1);
    /* --------------------------------------------- */
    const glimac::ShapeVertex* getDataPointer() const;
    const void * getVertex() const;
    GLsizei getVertexCount() const;
    /* --------------------------------------------- */

private:
    float m_length;
    std::vector<glimac::ShapeVertex> m_vertices;
    GLsizei m_vertexCount;
    VBO m_vbo;
    VAO m_vao;
};


