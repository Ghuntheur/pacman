#pragma once

#include <custom/Enum.hpp>

class Cell {

public:
    Cell(int x, int y, ContentType type);
    ContentType getType() const {
        return m_type;
    }

private:
    ContentType m_type;
    int m_x;
    int m_y;

};


