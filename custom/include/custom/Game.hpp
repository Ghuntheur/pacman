#pragma once

#define GLEW_STATIC
#include <iostream>
#include <GL/glew.h>
#include <glimac/SDLWindowManager.hpp>
#include <glimac/FreeflyCamera.hpp>
#include <glimac/FilePath.hpp>
#include <custom/Level.hpp>
#include <custom/Enum.hpp>
#include "custom/Characters/Pacman.hpp"
#include "custom/Characters/Ghost.hpp"
#include <glimac/Program.hpp>
#include <custom/Cube.hpp>


class Game {

public:
    explicit Game(glimac::FilePath path);

    /* -------------------------- */

    glimac::SDLWindowManager& getWindowManager();
    glimac::FreeflyCamera& getCamera();
    Level& getLevel();

    /* -------------------------- */

    void setGameState(GameState state);

    /* -------------------------- */

    void initGlew();
    void update();
    void play();
    void openWindow();

    // INITIALIZATIONS
    void initCameras();
    void keyDetection();


private:
    static GameState m_gameState;

    glimac::FilePath m_applicationPath;
    glimac::SDLWindowManager m_windowManager;
    glimac::FreeflyCamera m_camera;
    Level m_level;

    VBO m_vbo;
    VAO m_vao;

    Cube cube;
    Pacman pacman;
    Ghost ghost1;
    Ghost ghost2;
    Ghost ghost3;
    Ghost ghost4;
};



