#pragma once

#include <glimac/Sphere.hpp>
#include <custom/Object.hpp>

class Gum : Object {
protected:
    glimac::Sphere shape;
    GLfloat radius;

public:
    Gum(GLfloat rad) : radius(rad), shape(rad, 0.5, 0.5){}

};

