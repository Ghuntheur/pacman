#pragma once
#include "Gum.hpp"

class SuperGum: public Gum {
public:
    explicit SuperGum(GLfloat rad) : Gum(rad){}
};
