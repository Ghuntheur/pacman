#pragma once
#include "Character.hpp"
#include <SDL/SDL.h>
#include <glimac/SDLWindowManager.hpp>
#include <custom/VAO.hpp>
#include <custom/VBO.hpp>


class Pacman: public Character {
protected:
    unsigned int life;
    bool super;

public:
    Pacman();
    Pacman(unsigned int speed, const glm::vec2 &position, const glm::vec2 &respawnPos,
           unsigned int life, bool super) ;

    // getters
    unsigned int getLife() const;
    bool isSuper() const;

    // setters
    void setLife(unsigned int life);
    void setSuper(bool super);

    void powerUp();
    void moveLeft();
    void moveRight();
    void moveUp();
    void moveDown();

};
