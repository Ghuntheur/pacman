#pragma once
#include "Character.hpp"
#include "custom/Behaviors/GhostBehavior.hpp"

/** la classe  Ghost utilise une interface pour déterminer le comportement de déplacement
 *  les classes filles ont un comportement spécifique
 */
class Ghost: public Character {
private:
    bool afraid;
    GhostBehavior* behavior;

public:
    Ghost();
    Ghost(unsigned int speed, const glm::vec2 &position, const glm::vec2 &respawnPos,
          bool afraid, GhostBehavior * behavior);
    virtual ~Ghost();

    // getters
    bool isAfraid() const;
    const GhostBehavior &getBehavior() const;

    // setters
    void setAfraid(bool afraid);
    void setBehavior(GhostBehavior &behavior);


};
