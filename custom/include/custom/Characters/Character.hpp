#pragma once

#include <custom/Object.hpp>
#include "glimac/Sphere.hpp"
#include "glimac/glm.hpp"


class Character : public Object {
protected:
    unsigned int speed;
    glm::vec2 position;
    glm::vec2 respawnPos;
    glimac::Sphere shape;


public:
    Character();
    Character(unsigned int speed, const glm::vec2 &position, const glm::vec2 &respawnPos);
    virtual ~Character();

    // getters
    unsigned int getSpeed() const;
    const glm::vec2 &getPosition() const;
    const glm::vec2 &getRespawnPos() const;
    const glimac::Sphere &getShape() const;

    // setters
    void setSpeed(unsigned int speed);
    void setPosition(const glm::vec2 &position);
    void setRespawnPos(const glm::vec2 &respawnPos);

    const void * getVertex() const;
    GLsizei getVertexCount() const;
};
