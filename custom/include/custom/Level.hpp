#pragma once

#include <iostream>
#include <fstream>
#include <vector>
#include <list>
#include <string>
#include <sstream>
#include <custom/Cube.hpp>
#include <custom/Cell.hpp>


class Level {

public:
    explicit Level(unsigned int width = 0, unsigned int height = 0);

    /* --------------------------- */

    void getDimension();
    void build();
    void print();
    void draw() const;


private:
    std::ifstream m_file;
    unsigned int m_width;
    unsigned int m_height;
    Cell* buildCell(int x, int y, int type);
    std::vector<std::vector<Cell*>> m_map;
    std::list<Cube*> m_cubes;

};


