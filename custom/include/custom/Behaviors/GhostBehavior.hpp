#pragma once

/** la classe Behavior est abstraite
 *  les classes héritées implémentent concrètement les comportements spécifiques de chaque fantôme
 */
class GhostBehavior {
public:
    virtual void move() = 0; // make class abstract
};