#include "custom/Characters/Character.hpp"
// constructors / destructors
Character::Character() : shape(1, 32, 16) {}
Character::Character(unsigned int speed, const glm::vec2 &position, const glm::vec2 &respawnPos) : speed(speed), position(position), respawnPos(respawnPos), shape(1, 32, 16) {}

Character::~Character() {

}

// getters
unsigned int Character::getSpeed() const {
    return speed;
}
const glm::vec2 &Character::getPosition() const {
    return position;
}
const glm::vec2 &Character::getRespawnPos() const {
    return respawnPos;
}
const glimac::Sphere &Character::getShape() const {
    return shape;
}

// setters
void Character::setSpeed(unsigned int speed) {
    this->speed = speed;
}
void Character::setPosition(const glm::vec2 &position) {
    this->position = position;
}
void Character::setRespawnPos(const glm::vec2 &respawnPos) {
    this->respawnPos = respawnPos;
}

const void * Character::getVertex() const {
    return shape.getDataPointer();
}
GLsizei Character::getVertexCount() const {
    return shape.getVertexCount();
}


