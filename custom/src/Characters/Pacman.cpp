#include "custom/Characters/Pacman.hpp"

Pacman::Pacman(unsigned int speed, const glm::vec2 &position, const glm::vec2 &respawnPos,
               unsigned int life, bool super) : Character(speed, position, respawnPos), life(life),
                                                super(super) {}

// getters
unsigned int Pacman::getLife() const {
    return life;
}
bool Pacman::isSuper() const {
    return super;
}

// setters
void Pacman::setLife(unsigned int life) {
    Pacman::life = life;
}
void Pacman::setSuper(bool super) {
    Pacman::super = super;
}

Pacman::Pacman() {
    std::cout << "Pacman est initialisé" << std::endl;
}


void Pacman::powerUp() {

}

void Pacman::moveLeft() {

}

void Pacman::moveRight() {

}

void Pacman::moveUp() {

}

void Pacman::moveDown() {

}
