#include "custom/Characters/Ghost.hpp"
Ghost::Ghost() {
    std::cout << "Ghost est initialisé" << std::endl;
}

Ghost::Ghost(unsigned int speed, const glm::vec2 &position, const glm::vec2 &respawnPos,
             bool afraid, GhostBehavior * behavior) : Character(speed, position, respawnPos),
                                                           afraid(afraid), behavior(behavior) {}

Ghost::~Ghost() {

}

// getters
bool Ghost::isAfraid() const {
    return afraid;
}
const GhostBehavior &Ghost::getBehavior() const {
    return * behavior;
}

// setters
void Ghost::setAfraid(bool afraid) {
    this->afraid = afraid;
}
void Ghost::setBehavior(GhostBehavior &behavior) {
    this->behavior = &behavior;
}


