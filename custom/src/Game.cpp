#include <custom/Game.hpp>

// Init gamestate (static)
GameState Game::m_gameState = INITIALIZING;

Game::Game(const glimac::FilePath path)
        : m_applicationPath(path),
          m_windowManager(800, 800, "Pacman") { }

/* ----------------------------------- */


glimac::SDLWindowManager& Game::getWindowManager() {
    return this->m_windowManager;
}

glimac::FreeflyCamera& Game::getCamera() {
    return this->m_camera;
}

Level& Game::getLevel() {
    return this->m_level;
}

/* ----------------------------------- */

void Game::setGameState(const GameState state) {
    Game::m_gameState = state;
    this->update();
}

/* ----------------------------------- */

void Game::initGlew() {
    GLenum glewInitError = glewInit();
    if (GLEW_OK != glewInitError) {
        std::cerr << glewGetErrorString(glewInitError) << std::endl;
        return;
    }

    std::cout << "OpenGL Version : " << glGetString(GL_VERSION) << std::endl;
    std::cout << "GLEW Version : " << glewGetString(GLEW_VERSION) << std::endl;
}

void Game::update() {
    switch (Game::m_gameState) {
        case INITIALIZING:
            this->initGlew();
            this->setGameState(BUILDING);
            break;
        case BUILDING:
            this->m_level.build();
            this->setGameState(PLAYING);
            break;
        case PLAYING:
            this->play();
            this->setGameState(OVER);
            break;
        case OVER:
            break;
    }
}


void Game::keyDetection() {
    glimac::SDLWindowManager windowManager = this->getWindowManager();
    SDL_Event ev{};
    bool done = false;
    while (!done) {

        while (windowManager.pollEvent(ev)) {
            // quitter la fenetre
            if (ev.type == SDL_QUIT) {
                done = true;
            }

            // event
            switch(ev.type) {
                case SDL_MOUSEBUTTONDOWN:
                    switch(ev.button.button) {
                        case SDL_BUTTON_RIGHT:
                            break;
                        default:
                            break;
                    }
                    break;
                case SDL_MOUSEBUTTONUP:
                    switch(ev.button.button) {
                        case SDL_BUTTON_RIGHT:
                            break;
                        default:
                            break;
                    }
                    break;

                default:
                    break;

                case SDL_KEYDOWN:
                    switch (ev.key.keysym.sym) {
                        case SDLK_UP:
                            this->pacman.moveUp();
                            break;
                        case SDLK_DOWN:
                            this->pacman.moveDown();
                            break;
                        case SDLK_LEFT:
                            this->pacman.moveLeft();

                            break;
                        case SDLK_RIGHT:
                            this->pacman.moveRight();
                            break;
                        case SDLK_ESCAPE:
                            done = true;
                            break;
                        default:
                            break;
                    }
                    break;
            }

            /*if (clic) {
                float X = ev.motion.xrel;
                float Y = ev.motion.yrel;
                if (Y > 90) Y = 90;
                else if (Y < -90) Y = -90;*/
            }
        }

        /*glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        glUniformMatrix4fv(uMVPMatrixLoc, 1, GL_FALSE, glm::value_ptr(ProjMatrix*MVMatrix*cameraViewMatrix));
        glUniformMatrix4fv(uMVMatrixLoc, 1, GL_FALSE, glm::value_ptr(MVMatrix));
        glUniformMatrix4fv(uNormalMatrixLoc, 1, GL_FALSE, glm::value_ptr(NormalMatrix));


        cube.draw();

        windowManager.swapBuffers();*/
}






void Game::initCameras() {

}
/*
 * rajouter fonction
 * getModelMatrix
 *
 */
void Game::play() {
    this->openWindow();
    glimac::SDLWindowManager windowManager = this->getWindowManager();
    glimac::FreeflyCamera camera = this->getCamera();
    Level& level = this->getLevel();

    glimac::Program program = glimac::loadProgram(this->m_applicationPath.dirPath() + "shaders/3D.vs.glsl",
                                  this->m_applicationPath.dirPath() + "shaders/normals.fs.glsl");
    program.use();

    glEnable(GL_DEPTH_TEST);

    GLint uMVPMatrixLoc = glGetUniformLocation(program.getGLId(), "uMVPMatrix");
    GLint uMVMatrixLoc = glGetUniformLocation(program.getGLId(), "uMVMatrix");
    GLint uNormalMatrixLoc = glGetUniformLocation(program.getGLId(), "uNormalMatrix");

    glm::mat4 ProjMatrix, MVMatrix, NormalMatrix, MoonMVMatrix;
    ProjMatrix = glm::perspective(glm::radians(70.f), windowManager.getWidth()/windowManager.getHeight(), 0.1f, 100.f);
    MVMatrix = glm::translate(MVMatrix, glm::vec3(0.f, 0.f, -5.f));
    // NormalMatrix = glm::transpose(glm::inverse(MVMatrix));

    //glimac::Sphere sphere(1, 32, 16);
    //sphere.initBuffers();

//    Cube cube;
//    cube.initBuffers();
    pacman.init();

    bool done = false;
    bool clic = false;
    float motionSensitivity = 0.2;


    SDL_Event ev{};
    while (!done) {

        // get cam view matrix
        glm::mat4 cameraViewMatrix = camera.getViewMatrix();

        while (windowManager.pollEvent(ev)) {
            // quitter la fenetre
            if (ev.type == SDL_QUIT) {
                done = true;
            }

            // event
            switch(ev.type) {
                case SDL_MOUSEBUTTONDOWN:
                    switch(ev.button.button) {
                        case SDL_BUTTON_RIGHT:
                            clic = true;
                            break;
                        default:
                            break;
                    }
                    break;
                case SDL_MOUSEBUTTONUP:
                    switch(ev.button.button) {
                        case SDL_BUTTON_RIGHT:
                            clic = false;
                            break;
                        default:
                            break;
                    }
                    break;

                default:
                    break;

                case SDL_KEYDOWN:
                    switch (ev.key.keysym.sym) {
                        case SDLK_UP:
                            camera.moveFront(0.4f);
                            break;
                        case SDLK_DOWN:
                            camera.moveFront(-0.4f);
                            break;
                        case SDLK_LEFT:
                            camera.moveLeft(-0.4f);
                            break;
                        case SDLK_RIGHT:
                            camera.moveLeft(0.4f);
                            break;
                        case SDLK_ESCAPE:
                            done = true;
                            break;
                        default:
                            break;
                    }
                    break;
            }

            if (clic) {
                float X = ev.motion.xrel;
                float Y = ev.motion.yrel;
                if (Y > 90) Y = 90;
                else if (Y < -90) Y = -90;

                camera.rotateLeft(-X*motionSensitivity);
                camera.rotateUp(-Y*motionSensitivity);
            }
        }

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        glUniformMatrix4fv(uMVPMatrixLoc, 1, GL_FALSE, glm::value_ptr(ProjMatrix*MVMatrix*cameraViewMatrix));
        glUniformMatrix4fv(uMVMatrixLoc, 1, GL_FALSE, glm::value_ptr(MVMatrix));
        glUniformMatrix4fv(uNormalMatrixLoc, 1, GL_FALSE, glm::value_ptr(NormalMatrix));

        pacman.draw();
//        cube.draw();

        windowManager.swapBuffers();
    }

    pacman.removeBuffers();
//    cube.removeBuffers();
}

void Game::openWindow() {

}
