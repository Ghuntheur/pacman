#include "custom/Object.hpp"

void Object::init() {
    m_vao.init();
    m_vbo.init();

    // VBO
    this->m_vbo.bind();
    this->m_vbo.addVertices(this->getVertexCount(), this->getVertex());
    this->m_vbo.unbind();

    // VAO
    this->m_vao.bind();
    GLuint size = 3;
    this->m_vao.enableVertex(size, 0, 1, 2);
    this->m_vao.setOffset(size, 3, 3, 2);

    this->m_vbo.bind();
    this->m_vao.attribVertex(GL_FLOAT);
    this->m_vbo.unbind();

    this->m_vao.unbind();
}

/**
 * Remove VBO and VAO
 */
void Object::removeBuffers() {
    this->m_vbo.remove();
    this->m_vao.remove();
}

/**
 * Draw a cube
 */
void Object::draw() {
    this->m_vao.bind();
    glDrawArrays(GL_TRIANGLES, 0, this->getVertexCount());
}