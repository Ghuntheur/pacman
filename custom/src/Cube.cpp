#include <custom/Cube.hpp>

Cube::Cube(float length)
        : m_length(length) {
  this->build();
}

/* ------------------------------------------- */

void Cube::build() {

  this->m_vertexCount = 3*2*6;

  for (int i=0; i<this->m_vertexCount; i++) {
    this->m_vertices.push_back(glimac::ShapeVertex());
    this->m_vertices[i].normal = glm::vec3(1.f, 0.f, 0.f);
  }

  float half = this->m_length/2;

  // FRONT FACE
  this->m_vertices[0].position = glm::vec3(0.f-half, 0.f-half, 0.f-half);
  this->m_vertices[1].position = glm::vec3(1.f-half, 1.f-half, 0.f-half);
  this->m_vertices[2].position = glm::vec3(1.f-half, 0.f-half, 0.f-half);
  this->m_vertices[3].position = glm::vec3(0.f-half, 0.f-half, 0.f-half);
  this->m_vertices[4].position = glm::vec3(1.f-half, 1.f-half, 0.f-half);
  this->m_vertices[5].position = glm::vec3(0.f-half, 1.f-half, 0.f-half);

  // BACK FACE
  this->m_vertices[6].position = glm::vec3(0.f-half, 0.f-half, 1.f-half);
  this->m_vertices[7].position = glm::vec3(1.f-half, 1.f-half, 1.f-half);
  this->m_vertices[8].position = glm::vec3(1.f-half, 0.f-half, 1.f-half);
  this->m_vertices[9].position = glm::vec3(0.f-half, 0.f-half, 1.f-half);
  this->m_vertices[10].position = glm::vec3(1.f-half, 1.f-half, 1.f-half);
  this->m_vertices[11].position = glm::vec3(0.f-half, 1.f-half, 1.f-half);

  // UP FACE
  this->m_vertices[12].position = glm::vec3(0.f-half, 1.f-half, 0.f-half);
  this->m_vertices[13].position = glm::vec3(1.f-half, 1.f-half, 1.f-half);
  this->m_vertices[14].position = glm::vec3(1.f-half, 1.f-half, 0.f-half);
  this->m_vertices[15].position = glm::vec3(0.f-half, 1.f-half, 0.f-half);
  this->m_vertices[16].position = glm::vec3(1.f-half, 1.f-half, 1.f-half);
  this->m_vertices[17].position = glm::vec3(0.f-half, 1.f-half, 1.f-half);

  // BOTTOM FACE
  this->m_vertices[18].position = glm::vec3(0.f-half, 0.f-half, 0.f-half);
  this->m_vertices[19].position = glm::vec3(1.f-half, 0.f-half, 1.f-half);
  this->m_vertices[20].position = glm::vec3(1.f-half, 0.f-half, 0.f-half);
  this->m_vertices[21].position = glm::vec3(0.f-half, 0.f-half, 0.f-half);
  this->m_vertices[22].position = glm::vec3(1.f-half, 0.f-half, 1.f-half);
  this->m_vertices[23].position = glm::vec3(0.f-half, 0.f-half, 1.f-half);

  // LEFT FACE
  this->m_vertices[24].position = glm::vec3(0.f-half, 0.f-half, 0.f-half);
  this->m_vertices[25].position = glm::vec3(0.f-half, 1.f-half, 1.f-half);
  this->m_vertices[26].position = glm::vec3(0.f-half, 1.f-half, 0.f-half);
  this->m_vertices[27].position = glm::vec3(0.f-half, 0.f-half, 0.f-half);
  this->m_vertices[28].position = glm::vec3(0.f-half, 1.f-half, 1.f-half);
  this->m_vertices[29].position = glm::vec3(0.f-half, 0.f-half, 1.f-half);

  // RIGHT FACE
  this->m_vertices[30].position = glm::vec3(0.f-half, 0.f-half, 0.f-half);
  this->m_vertices[31].position = glm::vec3(0.f-half, 1.f-half, 1.f-half);
  this->m_vertices[32].position = glm::vec3(0.f-half, 1.f-half, 0.f-half);
  this->m_vertices[33].position = glm::vec3(0.f-half, 0.f-half, 0.f-half);
  this->m_vertices[34].position = glm::vec3(0.f-half, 1.f-half, 1.f-half);
  this->m_vertices[35].position = glm::vec3(0.f-half, 0.f-half, 1.f-half);
}

/* ------------------------------------------- */

/**
 * Return all vertices to drawn cube
 * @return vertices
 */
const glimac::ShapeVertex* Cube::getDataPointer() const {
  return &this->m_vertices[0];
}

/**
 * Return number of vertices for a cube
 * @return vertices number
 */
GLsizei Cube::getVertexCount() const {
  return this->m_vertexCount;
}

/* ------------------------------------------- */

const void * Cube::getVertex() const {
    return this->getDataPointer();
}

