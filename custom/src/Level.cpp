#include <custom/Level.hpp>

Level::Level(unsigned int width, unsigned int height) : m_width(width), m_height(height) {
    m_file.open("level/level.csv");
    getDimension();
}

/* ------------------------------- */

void Level::getDimension() {
    std::string line;

    while (std::getline(m_file, line)) {
        m_height++;
    }

    std::stringstream iss(line);
    while (std::getline(iss, line, ' ')) {
        m_width++;
    }

    m_map.resize(m_height, std::vector<Cell*>(m_width, 0));
}

void Level::build() {
    m_file.clear();
    m_file.seekg(0, std::ios_base::beg);

    std::string line;

    for (int i=0; i<m_height; i++) {
        std::getline(m_file, line);
        std::stringstream iss(line);
        for (int j=0; j<m_width; j++) {
            std::string val;
            std::getline(iss, val, ' ');
            std::stringstream data(val);

            // fill cube list and map
            if (val == "1") {
                m_cubes.push_back(new Cube);
                m_map[i][j] = new Cell(i, j, WALL);
            }
            else
                m_map[i][j] = new Cell(i, j, EMPTY);
        }
    }

    print();
}

Cell* Level::buildCell(int x, int y, int type) {
  switch (type) {
    case 0:
      return new Cell(x, y, EMPTY);
    case 1:
      return new Cell(x, y, WALL);
    default:
      break;
  }
}

void Level::print() {
    for (int i=0; i<m_height; i++) {
        for (int j=0; j<m_width; j++) {
            std::cout << m_map[i][j]->getType() << " ";
        }
        std::cout << std::endl;
    }
    std::cout << "There are " << m_cubes.size() << " cubes in this level" << std::endl;
}

void Level::draw() const {
    int temp = 0;
    for (int i = 0; i <= m_width; i++) {
        for (int j = 0; j <= m_height; j++) {

        }
    }
}

//ProjMatrix = glm::perspective( (float) glm::radians(70.f), 1.f, 0.1f, 100.f );
//MVMatrix = glm::translate(glm::mat4(), glm::vec3(0, 0, -5)); // Translation

// NormalMatrix = glm::transpose(glm::inverse(MVMatrix));

// Envoi des matrices
/*
glUniformMatrix4fv(MVPM_loc, 1, GL_FALSE, glm::value_ptr(ProjMatrix * MVMatrix));
glUniformMatrix4fv(MVM_loc, 1, GL_FALSE, glm::value_ptr(MVMatrix));
glUniformMatrix4fv(NM_loc, 1, GL_FALSE, glm::value_ptr(NormalMatrix));*/
