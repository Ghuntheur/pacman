#pragma once

#include <cstdint>
#include <SDL/SDL.h>
#include "glm.hpp"

namespace glimac {

class SDLWindowManager {
public:
    SDLWindowManager(float width = 800, float height = 600, const char* title = "title");

    ~SDLWindowManager();

    bool pollEvent(SDL_Event& e);

    bool isKeyPressed(SDLKey key) const;

    // button can SDL_BUTTON_LEFT, SDL_BUTTON_RIGHT and SDL_BUTTON_MIDDLE
    bool isMouseButtonPressed(uint32_t button) const;

    glm::ivec2 getMousePosition() const;

    void swapBuffers();

    // Return the time in seconds
    float getTime() const;

    float getWidth() const;
    float getHeight() const;

private:
    float m_width;
    float m_height;
};

}
