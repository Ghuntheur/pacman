#define GLEW_STATIC
#include <iostream>
#include <GL/glew.h>
#include <SDL/SDL.h>
#include <glimac/Program.hpp>
#include <glimac/Sphere.hpp>
#include <glimac/SDLWindowManager.hpp>
#include <glimac/Geometry.hpp>

#include <custom/Game.hpp>
#include <custom/Cube.hpp>
#include <custom/Level.hpp>

using namespace glimac;

int main(int argc, char** argv) {

    glimac::FilePath applicationPath(argv[0]);
    Game game(applicationPath);
    game.update();

    return EXIT_SUCCESS;
}

